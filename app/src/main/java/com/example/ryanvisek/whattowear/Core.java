package com.example.ryanvisek.whattowear;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.*;



public class Core {
    // Bitly OAuth access token (a unique ID).
    // You can register for your own access token, or use the one here.
    private final String ACCESS_TOKEN = "ca51a08ea2deeeca";
    private JsonElement jse, jse2,jse3;

    public Core(String ZIP) {
        jse = null;
        jse2 = null;

        try {

            // Construct Bitly API URL
            URL URL1 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/conditions/q/"
                    + ZIP + ".json");

            // Open the URL
            InputStream is = URL1.openStream(); // throws an IOException
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Read the result into a JSON Element
            jse = new JsonParser().parse(br);

            //second URL for the forecast

            String city = jse.getAsJsonObject().get("current_observation")
                    .getAsJsonObject().get("display_location")
                    .getAsJsonObject().get("city").getAsString();
            String State = jse.getAsJsonObject().get("current_observation")
                    .getAsJsonObject().get("display_location")
                    .getAsJsonObject().get("state").getAsString();

            String city1 = city.replaceAll(" ", "_");
            URL URL2 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/forecast10day/q/" + State + "/" + city1 + ".json");

            InputStream is2 = URL2.openStream();
            BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));

            jse2 = new JsonParser().parse(br2);






            URL URL3 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/radar/q/" + ZIP + ".json");

            InputStream is3 = URL3.openStream();
            BufferedReader br3 = new BufferedReader(new InputStreamReader(is3));

            jse3 = new JsonParser().parse(br3);

            is3.close();
            br3.close();

            is2.close();
            br2.close();

            is.close();
            br.close();




        }

        catch (java.io.UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        } catch (java.net.MalformedURLException mue)
        {
            mue.printStackTrace();
        } catch (java.io.IOException ioe)
        {
            ioe.printStackTrace();
        }
        //return null;
    }

    public String getWeather()
    {

        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("weather").getAsString();

    }

    public String getTemperature()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("temp_f").getAsString();
    }

    public String getCity()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("full").getAsString();
    }
    public int getZIP()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("zip").getAsInt();
    }

    public String getLastUpdated()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("observation_time").getAsString();
    }

    public String getFeelsLike()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("feelslike_f").getAsString();
    }

    public String getWindMph()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("wind_mph").getAsString();
    }
    public String gethumidity()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("relative_humidity").getAsString();
    }
    /*public String getIconURL()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("icon_url").getAsString();
    }

    public String getIcon()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("icon").getAsString();

    }*/
    public String getIconURL()
    {
        if (getIcon().equals("clear")|| getIcon().equals("mostlysunny"))
        {
            return "http://www.iconsdb.com/icons/preview/white/sun-4-xl.png";
        }
        else if(getIcon().equals("cloudy"))
        {
            return "http://www.iconsdb.com/icons/preview/white/cloud-2-xl.png";
        }
        else if(getIcon().equals("chancerain")|| getIcon().equals("rain"))
        {
            return "http://www.iconsdb.com/icons/preview/white/rain-xl.png";
        }
        else if(getIcon().equals("chancetstorms") || getIcon().equals("tstorms"))
        {
            return "http://www.iconsdb.com/icons/preview/white/storm-2-xl.png";
        }
        else if(getIcon().equals("chancesleet") || getIcon().equals("sleet"))
        {
            return "http://www.iconsdb.com/icons/preview/white/sleet-xl.png";
        }
        else if(getIcon().equals("chanceflurries")|| getIcon().equals("chancesnow")|| getIcon().equals("flurries"))
        {
            return "http://www.iconsdb.com/icons/preview/white/snow-storm-xl.png";
        }
        else if(getIcon().equals("fog"))
        {
            return "http://www.iconsdb.com/icons/preview/white/fog-day-xl.png";
        }
        else if(getIcon().equals("hazy"))
        {
            return "http://www.iconsdb.com/icons/preview/white/dust-xl.png";
        }
        else if(getIcon().equals("partlycloudy")||getIcon().equals("partlysunny"))
        {
            return "http://www.iconsdb.com/icons/preview/white/partly-cloudy-day-xl.png";
        }

        return "http://www.iconsdb.com/icons/preview/white/cloud-2-xl.png";
    }
    public String getIcon5URL(int v)
    {
        if (getIcon5(v).equals("clear")|| getIcon5(v).equals("mostlysunny"))
        {
            return "http://www.iconsdb.com/icons/preview/white/sun-4-xl.png";
        }
        else if(getIcon5(v).equals("cloudy"))
        {
            return "http://www.iconsdb.com/icons/preview/white/cloud-2-xl.png";
        }
        else if(getIcon5(v).equals("chancerain")|| getIcon5(v).equals("rain"))
        {
            return "http://www.iconsdb.com/icons/preview/white/rain-xl.png";
        }
        else if(getIcon5(v).equals("chancetstorms") || getIcon5(v).equals("tstorms"))
        {
            return "http://www.iconsdb.com/icons/preview/white/storm-2-xl.png";
        }
        else if(getIcon5(v).equals("chancesleet")|| getIcon5(v).equals("sleet"))
        {
            return "http://www.iconsdb.com/icons/preview/white/sleet-xl.png";
        }
        else if(getIcon5(v).equals("chanceflurries")|| getIcon5(v).equals("chancesnow")|| getIcon5(v).equals("flurries"))
        {
            return "http://www.iconsdb.com/icons/preview/white/snow-storm-xl.png";
        }
        else if(getIcon5(v).equals("fog"))
        {
            return "http://www.iconsdb.com/icons/preview/white/fog-day-xl.png";
        }
        else if(getIcon5(v).equals("hazy"))
        {
            return "http://www.iconsdb.com/icons/preview/white/dust-xl.png";
        }
        else if(getIcon5(v).equals("partlycloudy")||getIcon5(v).equals("partlysunny"))
        {
            return "http://www.iconsdb.com/icons/preview/white/partly-cloudy-day-xl.png";
        }

        return "http://www.iconsdb.com/icons/preview/white/cloud-2-xl.png";
    }

    public String getIcon()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("icon").getAsString();


    }
    public String getIcon5(int v)
    {
        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("icon")
                .getAsString();
        return a;
    }

    public String getHowMuchRain()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("precip_today_string").getAsString();
    }

    public String getForecastDay(int v)
    {

        String b = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("date").getAsJsonObject()
                .get("weekday").getAsString();

        return b;

    }
    public String getForecastDayHigh(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("high").getAsJsonObject()
                .get("fahrenheit").getAsString();

        return a;
    }
    public String getForecastDayLow(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("low").getAsJsonObject()
                .get("fahrenheit").getAsString();
        return a;
    }
    public String getForecastDayWeather(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("conditions")
                .getAsString();
        return a;
    }
    public String getForecastDayWind(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("avewind").getAsJsonObject()
                .get("mph").getAsString();
        return a;
    }

    public String getRadarURL()
    {


        String radarURL = "http://api.wunderground.com/api/ca51a08ea2deeeca/animatedradar/animatedsatellite/"
                +"q/" +getZIP()+".gif?num=6&delay=50&interval=30";

        return radarURL;

    }
    public  Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String clothes()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<50)
        {
            return "heavy jacket & pants today.";
        }
        else if(foo<60 && foo>=50)
        {
            return "sweatshirt & pants today.";
        }
        else if(foo<70&&foo>=60)
        {
            return "bring a light jacket";
        }
        else if(foo<80&&foo>=70)
        {
            return "t-shirt & shorts today.";
        }
        else if(foo<110&&foo>=80)
        {
            return "tank top & shorts today.";
        }
        return "go naked";
    }
    public String getClothesIcon()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<50)
        {
            return "heavyjacket";
        }
        else if(foo<60 && foo>=50)
        {
            return "sweatshirt";
        }
        else if(foo<70&&foo>=60)
        {
            return "lightjacket";
        }
        else if(foo<80&&foo>=70)
        {
            return "shirt";
        }
        else if(foo<110&&foo>=80)
        {
            return "tanktop";
        }
        return "shirt";
    }
    public String umb()
    {
        String a = getHowMuchRain();
        String b = a.replaceAll("[^0-9]","");
        double foo = Double.parseDouble(b);
        if(foo>0) {
            return "bring umbrella";
        }
        return "";
    }
    public int getBackground1()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<60)
        {
            return 31;
        }

        else if(foo<79&&foo>=60)
        {
            return 30;
        }

        else if(foo<150&&foo>=79)
        {
            return 248;
        }
        return 21;
    }
    public int getBackground3()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<60)
        {
            return 147;
        }

        else if(foo<79&&foo>=60)
        {
            return 195;
        }

        else if(foo<150&&foo>=79)
        {
            return 6;
        }
        return 154;
    }
    public int getBackground2()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<60)
        {
            return 58;
        }

        else if(foo<79&&foo>=60)
        {
            return 139;
        }

        else if(foo<150&&foo>=79)
        {
            return 148;
        }
        return 60;
    }
    }


   /* public static void main(String[] args) {
        Core a = new Core("95747");
        System.out.println("City: "+ a.getCity());
        System.out.println("Current temp: " +a.getTemperature());
        System.out.println("Current conditions: " + a.getWeather());
        System.out.println("When last update: " +a.getLastUpdated());
        System.out.println("Current feels like: " + a.getFeelsLike());
        System.out.println("Wind MPH: " + a.getWindMph());
        System.out.println("Icon to use: " + a.getIcon());
        System.out.println("How much rain: " + a.getHowMuchRain());
        System.out.println("High: " + a.getForecastDayHigh(0));
        System.out.println("Low: " + a.getForecastDayLow(0));
        System.out.println("Weather Conditions: " + a.getForecastDayWeather(0));
        System.out.println("Certain day Wind MPH: " +a.getForecastDayWind(0));
        System.out.println("Weekday: " + a.getForecastDay(0));
        System.out.println(a.getRadarURL());

    }*/


