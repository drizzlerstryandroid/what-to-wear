package com.example.ryanvisek.whattowear;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Forecast extends Fragment {

    TextView textView;


    public Forecast() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        //textView = (TextView) view.findViewById(R.id.textView2);
        Bundle bundle = getArguments();
        String message = Integer.toString(bundle.getInt("count"));
        return view;

    }
}
