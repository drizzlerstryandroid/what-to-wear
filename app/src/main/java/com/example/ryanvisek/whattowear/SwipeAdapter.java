package com.example.ryanvisek.whattowear;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapter extends FragmentPagerAdapter {
    public SwipeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override //gets the position and switches the fragment depending on the screen the user is on
    public Fragment getItem(int position) {
        Bundle bundle;
        Fragment fragment = null;
        switch(position){
            case 0 : fragment = new CurrentConditions();
                 bundle = new Bundle();
                bundle.putInt("count", position + 1);
                fragment.setArguments(bundle);
                break;
            case 1 :  fragment = new Forecast();
                bundle = new Bundle();
                bundle.putInt("count", position + 1);
                fragment.setArguments(bundle);
                break;
            /*case 2 :  fragment = new Radar();
                bundle = new Bundle();
                bundle.putInt("count", position + 1);
                fragment.setArguments(bundle);
                break;*/

        }
        return fragment;




    }


    @Override //controls how many screen are available to swipe
    public int getCount() {

        return 2;
    }
}
