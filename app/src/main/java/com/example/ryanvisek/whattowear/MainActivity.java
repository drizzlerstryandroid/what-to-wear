package com.example.ryanvisek.whattowear;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;


public class MainActivity extends FragmentActivity {
    private char degree = '\u00B0';
    Core c;

    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setBackgroundColor(Color.rgb(0,34,62));
        SwipeAdapter swipeadapter = new SwipeAdapter(getSupportFragmentManager());
        viewPager.setAdapter(swipeadapter);
    }
    private class GetWeatherInBackground extends AsyncTask<String, Void, Bitmap[] >
    {
        @Override
        protected Bitmap[] doInBackground(String...locations)
        {
            // Fetch the weather data

                try {
                    c = new Core(locations[0]);
                    Bitmap[] bmaparray = new Bitmap[7];
                    Bitmap bmap1 = c.getBitmapFromURL(c.getClothesIcon());
                    Bitmap bmap2 = c.getBitmapFromURL(c.getIconURL());
                    Bitmap bmap3 = c.getBitmapFromURL(c.getIcon5URL(1));
                    Bitmap bmap4 = c.getBitmapFromURL(c.getIcon5URL(2));
                    Bitmap bmap5 = c.getBitmapFromURL(c.getIcon5URL(3));
                    Bitmap bmap6 = c.getBitmapFromURL(c.getIcon5URL(4));
                    Bitmap bmap7 = c.getBitmapFromURL(c.getIcon5URL(5));
                    bmaparray[0] = bmap1;
                    bmaparray[1] = bmap2;
                    bmaparray[2] = bmap3;
                    bmaparray[3] = bmap4;
                    bmaparray[4] = bmap5;
                    bmaparray[5] = bmap6;
                    bmaparray[6] = bmap7;




                    return bmaparray;
                }
                catch(Exception e){
                    Bitmap [] bmaparray = new Bitmap[7];
                    e.printStackTrace();
                    return bmaparray;
                }








        }


        @Override
        protected void onPostExecute(Bitmap [] bmaparray)
        {
            String temp = c.getTemperature();

            Typeface helvy = Typeface.createFromAsset(getAssets(), "font/helvy.ttf");
            Typeface bebas = Typeface.createFromAsset(getAssets(), "font/bebas.ttf");
            ViewPager main = (ViewPager) findViewById(R.id.view_pager);
            main.setBackgroundColor(Color.rgb(c.getBackground1(),c.getBackground2(),
                                                c.getBackground3()));



            TextView tempField = (TextView) findViewById(R.id.tempF);
            TextView locationField = (TextView) findViewById(R.id.location);
            TextView feelslikeField = (TextView) findViewById(R.id.feelslike);
            TextView windField = (TextView) findViewById(R.id.wind);
            TextView clothesField = (TextView) findViewById(R.id.clothes);
            TextView header1Field = (TextView) findViewById(R.id.Header1);
            TextView header2Field = (TextView) findViewById(R.id.Header2);
            TextView amtofrain = (TextView) findViewById(R.id.chanceofrain);
            TextView humid = (TextView) findViewById(R.id.humidity);


            header1Field.setTypeface(helvy);
            header2Field.setTypeface(helvy);
            tempField.setTypeface(bebas);
            amtofrain.setTypeface(helvy);
            humid.setTypeface(helvy);
            clothesField.setTypeface(helvy);
            windField.setTypeface(helvy);
            feelslikeField.setTypeface(helvy);
            locationField.setTypeface(helvy);




            header1Field.setText("What to Wear");
            header2Field.setText("Today's Weather");
            tempField.setText(temp + degree + "F");
            locationField.setText("It's " + c.getWeather() + " in " + c.getCity()+ ".");
            feelslikeField.setText("FeelsLike : "+c.getFeelsLike() + degree+"F");
            windField.setText(c.getWindMph() + " MPH winds");
            clothesField.setText(c.clothes());
            amtofrain.setText(c.getHowMuchRain()+ " of rain today");
            humid.setText(c.gethumidity() + " Humidity");

            String a = c.getClothesIcon();
            int id = getResources().getIdentifier("com.example.ryanvisek.whattowear:drawable/" + a, null, null);


            Bitmap clothesb = bmaparray[0];
            Bitmap ccbmap = bmaparray[1];
            ImageView currentimage = (ImageView) findViewById(R.id.currentView);
            currentimage.setImageBitmap(ccbmap);
            ImageView clothesimage = (ImageView) findViewById(R.id.clothesView);
            clothesimage.setImageResource(id);

            ImageView iday1 = (ImageView) findViewById(R.id.day1image);
            ImageView iday2 = (ImageView) findViewById(R.id.day2image);
            ImageView iday3 = (ImageView) findViewById(R.id.day3image);
            ImageView iday4 = (ImageView) findViewById(R.id.day4image);
            ImageView iday5 = (ImageView) findViewById(R.id.day5image);

            Bitmap bm1 = bmaparray[2];
            Bitmap bm2 = bmaparray[3];
            Bitmap bm3 = bmaparray[4];
            Bitmap bm4 = bmaparray[5];
            Bitmap bm5 = bmaparray[6];


            iday1.setImageBitmap(bm1);
            iday2.setImageBitmap(bm2);
            iday3.setImageBitmap(bm3);
            iday4.setImageBitmap(bm4);
            iday5.setImageBitmap(bm5);







            String day1 = c.getForecastDay(1);
            String day2 = c.getForecastDay(2);
            String day3 = c.getForecastDay(3);
            String day4 = c.getForecastDay(4);
            String day5 = c.getForecastDay(5);
            String temp1 = c.getForecastDayHigh(1);
            String temp2 = c.getForecastDayHigh(2);
            String temp3 = c.getForecastDayHigh(3);
            String temp4 = c.getForecastDayHigh(4);
            String temp5 = c.getForecastDayHigh(5);
            String temp1l = c.getForecastDayLow(1);
            String temp2l = c.getForecastDayLow(2);
            String temp3l = c.getForecastDayLow(3);
            String temp4l = c.getForecastDayLow(4);
            String temp5l = c.getForecastDayLow(5);


            TextView day1Field = (TextView)findViewById(R.id.day1);
            TextView day2Field = (TextView)findViewById(R.id.day2);
            TextView day3Field = (TextView)findViewById(R.id.day3);
            TextView day4Field = (TextView)findViewById(R.id.day4);
            TextView day5Field = (TextView)findViewById(R.id.day5);

            day1Field.setTypeface(helvy);
            day2Field.setTypeface(helvy);
            day3Field.setTypeface(helvy);
            day4Field.setTypeface(helvy);
            day5Field.setTypeface(helvy);

            TextView temp1Field = (TextView)findViewById(R.id.temp1);
            TextView temp2Field = (TextView)findViewById(R.id.temp2);
            TextView temp3Field = (TextView)findViewById(R.id.temp3);
            TextView temp4Field = (TextView)findViewById(R.id.temp4);
            TextView temp5Field = (TextView)findViewById(R.id.temp5);

            temp1Field.setTypeface(helvy);
            temp2Field.setTypeface(helvy);
            temp3Field.setTypeface(helvy);
            temp4Field.setTypeface(helvy);
            temp5Field.setTypeface(helvy);



            day1Field.setText("" + day1);
            day2Field.setText("" + day2);
            day3Field.setText("" + day3);
            day4Field.setText("" + day4);
            day5Field.setText("" + day5);

            temp1Field.setText("High: " + temp1 + degree + " // Low: " + temp1l + degree);
            temp2Field.setText("High: " + temp2 + degree + " // Low: " + temp2l + degree);
            temp3Field.setText("High: " + temp3 + degree + " // Low: " + temp3l + degree);
            temp4Field.setText("High: " + temp4 + degree + " // Low: " + temp4l + degree);
            temp5Field.setText("High: " + temp5 + degree + " // Low: " + temp5l + degree);


            WebView rImage = (WebView) findViewById(R.id.rImage);
            rImage.getSettings().setJavaScriptEnabled(true);
            rImage.loadUrl(c.getRadarURL());
            rImage.setVisibility(View.VISIBLE);


           /* ViewPager main = (ViewPager)  findViewById(R.id.view_pager);
            View root = main.getRootView();
            root.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));*/




        }
    }

    public void fetch(View v)
    {
        TextView header1 = (TextView) findViewById(R.id.Header1);
        EditText zip = (EditText) findViewById(R.id.zip);
        String zip1 = zip.getText().toString();
        try {
            new GetWeatherInBackground().execute(zip1);
        }
        catch(Exception e )
        {

            header1.setText("Invalid Zip");



        }


    }






}


